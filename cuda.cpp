#include <iomanip>
#include <iostream>
#include <sstream>

#include <unistd.h>

#include "gpu.hpp"

std::string get_hostname() {
    const int maxlen = 128;
    char name[maxlen];
    if( gethostname(name, maxlen) ) {
        std::cerr << "error finding host name" << std::endl;
        exit(1);
    }

    return std::string(name);
}

int main(int argc, char **argv) {
    auto hostname = get_hostname();
    auto gpus = get_gpu_uuids();
    auto num_gpus = gpus.size();

    std::cout << "GPU affinity test: " << num_gpus << " GPUs\n";

    for(auto i=0; i<num_gpus; ++i) {
        std::cout << " gpu " << std::setfill(' ') << std::setw(3) << i << " : GPU-" << gpus[i] << "\n";
    }
}

