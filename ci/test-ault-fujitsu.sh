#! /bin/bash

base_path="$1"
host="$(hostname)"
echo "=== path: $base_path"
echo "=== host: $host"

echo "=== loading environment"
source /users/bcumming/a64fx/env.sh
spack load git gcc@11.1.0 cmake ninja

build_path="$base_path/build"
echo "=== building: $build_path"
mkdir "$build_path"
cd "$build_path"
CC=gcc CXX=g++ cmake .. -G Ninja
ninja

bin_path="$build_path"
echo "=== testing: $bin_path/affinity.omp"
cd "$bin_path"
./affinity.omp
