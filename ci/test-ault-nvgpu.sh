#! /bin/bash

base_path="$1"
host="$(hostname)"
echo "=== path: $base_path"
echo "=== host: $host"

echo "=== loading environment"
module load cmake/3.18.2 cuda

build_path="$base_path/build-nvgpu"
echo "=== building: $build_path"
mkdir "$build_path"
cd "$build_path"
CC=gcc CXX=g++ cmake .. -G Ninja
ninja

bin_path="$build_path"
echo "=== testing: $bin_path/affinity.omp"
cd "$bin_path"
./affinity.gpu
